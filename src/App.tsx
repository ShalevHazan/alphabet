import React from "react";
import logo from "./logo.svg";
import "./App.css";
import NBAList from "./components/NBAListComponent";
import FavoriteList from "./components/FavoriteListComponent";

function App() {
  return (
    <div className="App">
      <div style={{ display: "flex", flexDirection: "row" }}>
        <NBAList />
        <FavoriteList />
      </div>
    </div>
  );
}

export default App;
