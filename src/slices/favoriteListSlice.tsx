import { createSlice } from "@reduxjs/toolkit";
import { Player } from "../types/Player";

type InitialState = {
  FavoriteList: Player[];
};

const initialState: InitialState = {
  FavoriteList: [],
};

export const favoriteListSlice = createSlice({
  name: "FavoriteList",
  initialState: initialState,
  reducers: {
    AddToList: (
      state,
      action: {
        payload: Player;
        type: string;
      }
    ) => {
      return {
        ...state,
        FavoriteList: [...state.FavoriteList, action.payload],
      };
    },
    RemoveFromList: (
      state,
      action: {
        payload: number;
        type: string;
      }
    ) => {
      return {
        ...state,
        FavoriteList: state.FavoriteList.filter(
          (player: { id: number }) => player.id !== action.payload
        ),
      };
    },
  },
});

export const { AddToList, RemoveFromList } = favoriteListSlice.actions;
export default favoriteListSlice.reducer;
