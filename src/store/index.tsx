import { configureStore } from "@reduxjs/toolkit";
import favoriteListReducer from "../slices/favoriteListSlice";

export const store = configureStore({
  reducer: {
    favorites: favoriteListReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
