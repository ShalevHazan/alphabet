import React, { useEffect, useState } from "react";

import PlayerComponent from "../PlayerComponent";

import { Player } from "../../types/Player";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { AddToList } from "../../slices/favoriteListSlice";

import { listStyle, listContainerStyle } from "./styles";

const NBAList: React.FunctionComponent = () => {
  const [playersList, setPlayerList] = useState<Player[]>([]);
  const [search, setSearch] = useState<string>("");

  const favoriteList = useSelector(
    (state: RootState) => state.favorites.FavoriteList
  );

  const dispatch = useDispatch();

  const isFavorite = (player: Player) => {
    return favoriteList.some((Favplayer: Player) => Favplayer.id === player.id);
  };

  const addPlayerToFavoriteList = (player: Player) => {
    if (!isFavorite(player)) {
      dispatch(AddToList(player));
    }
  };
  useEffect(() => {
    fetch("https://www.balldontlie.io/api/v1/players")
      .then((res) => res.json())
      .then((JsonData) => {
        setPlayerList(JsonData.data);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  return (
    <div style={listStyle}>
      All Players:
      <div>
        <input
          type="text"
          placeholder="search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <div style={listContainerStyle}>
        {playersList.map((player: Player) => {
          if (
            search == "" ||
            player.first_name.toLowerCase().includes(search.toLowerCase())
          ) {
            return (
              <PlayerComponent
                key={player.id}
                player={player}
                buttonString="Add to Favorites"
                action={addPlayerToFavoriteList}
              />
            );
          }
        })}
      </div>
    </div>
  );
};

export default NBAList;
