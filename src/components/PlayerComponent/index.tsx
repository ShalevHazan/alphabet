import React, { useEffect, useState } from "react";

import { Player } from "../../types/Player";

import { playerContainerStyle, buttonStyle } from "./styles";

type PlayerComponentType = {
  player: Player;
  buttonString: string;
  action: (player: Player) => void;
};

const PlayerComponent: React.FunctionComponent<PlayerComponentType> = ({
  player,
  buttonString,
  action,
}) => {
  return (
    <div style={playerContainerStyle}>
      {player.first_name} {player.last_name}
      <button style={buttonStyle} onClick={() => action(player)}>
        {buttonString}
      </button>
    </div>
  );
};

export default PlayerComponent;
