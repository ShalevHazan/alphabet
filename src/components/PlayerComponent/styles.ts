import { Properties } from 'csstype';


export const playerContainerStyle: Properties = {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: '1px',
    borderStyle: "solid",
    borderColor: "black",
    height: "50px",
  };
  
export const buttonStyle: Properties = { height: "70%", marginRight: '10px' }