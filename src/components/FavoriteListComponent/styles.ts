import { Properties } from 'csstype';


export const listStyle: Properties = {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
  };
  
export const listContainerStyle: Properties = {
    width: "40%",
    marginBottom: '10px',
    marginTop: '10px',
  }