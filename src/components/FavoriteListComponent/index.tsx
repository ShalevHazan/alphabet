import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PlayerComponent from "../PlayerComponent";

import { Player } from "../../types/Player";
import { RootState } from "../../store";
import { RemoveFromList } from "../../slices/favoriteListSlice";

import { listStyle, listContainerStyle } from "./styles";
const FavoriteListComponent: React.FunctionComponent = () => {
  const favoriteList = useSelector(
    (state: RootState) => state.favorites.FavoriteList
  );

  const [color, setColor] = useState<string>("white");
  const [backGround, setBackGround] = useState<string>("");

  const dispatch = useDispatch();

  const removePlayerToFavoriteList = (player: Player) => {
    dispatch(RemoveFromList(player.id));
  };

  return (
    <div style={listStyle}>
      Favorite Players:
      <div style={{ ...listContainerStyle, backgroundColor: backGround }}>
        {favoriteList.map((player: Player) => (
          <PlayerComponent
            key={player.id}
            player={player}
            buttonString="Remove from Favorites"
            action={removePlayerToFavoriteList}
          />
        ))}
      </div>
      choose color:
      <input
        type="text"
        placeholder="color"
        value={color}
        onChange={(e) => setColor(e.target.value)}
      />
      <button onClick={() => setBackGround(color)}>
        Change background color
      </button>
    </div>
  );
};

export default FavoriteListComponent;
